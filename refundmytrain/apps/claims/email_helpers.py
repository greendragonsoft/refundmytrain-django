from django.core.mail import EmailMessage
from django.template import loader


def send_confirmation_email(claim):
    """
    Send a claim confirmation to the actual customer.
    """

    # TODO: move this to template
    # "claims/emails/subject_send_confirmation_email.txt"
    subject = "{} to {} on {} (reference {})".format(
        claim.travelling_from,
        claim.travelling_to,
        claim.journey_date.strftime('%A %d %B'),
        claim.reference
    )

    template = loader.get_template(
        'claims/email/confirm_claim_to_customer.txt')

    message = template.render({"claim": claim})
    from_address = 'RefundMyTrain.com <confirmation@refundmytrain.com>'

    recipient_list = [
        '{} <{}>'.format(claim.your_name, claim.your_email)
    ]

    email = EmailMessage(
        subject, message, from_address, recipient_list,
        ['customer-confirmation-bcc@refundmytrain.com'],
        reply_to=['help@refundmytrain.com'],
        attachments=[claim.scanned_ticket_as_attachment_triplet()],
    )
    email.send()


def send_email_to_train_company(
        claim, sender, to, subject, bcc, message, attach_ticket):

    email = EmailMessage(subject, message, sender, [to], [bcc])
    if attach_ticket:
        email.attachments = [claim.scanned_ticket_as_attachment_triplet()]

    email.send()
