import os
import random
import re
import uuid

import luhn

from django.db import models

from versatileimagefield.fields import VersatileImageField


def make_scan_filename(instance, filename):
    if instance.pk:
        name = "claim-{}".format(instance.pk)
    else:
        name = 'unknown-{}'.format(uuid.uuid4())

    return 'tickets/{name}{extension}'.format(
        name=name,
        extension=os.path.splitext(filename)[1]
    )


def generate_reference():
    """
    Return a luhn-valid 10-digit reference like:
    123-456-7890

    Where the final digit is a check digit.
    """

    characters = '0123456789'

    for attempt in range(10):
        nine_digits = ''.join(random.choice(characters) for _ in range(9))
        reference = '{}-{}-{}{}'.format(
            nine_digits[0:3],
            nine_digits[3:6],
            nine_digits[6:9],
            luhn.generate(nine_digits))

        try:
            Claim.objects.get(reference=reference)
        except Claim.DoesNotExist:
            return reference

    raise RuntimeError('Failed repeatedly to generate a unique reference.')


class Claim(models.Model):
    STATE_CHOICES = (
        ('INCOMPLETE', 'Incomplete'),
        ('AWAITING_EMAIL_CONFIRMATION', (
            'Awaiting email confirmation: please click the link in the email '
            'we sent you.')),
        ('INTERNAL_REVIEW', ('Awaiting one of our team to review before '
                             'sending to the train operating company.')),
        ('SUBMITTED', 'Submitted to operating company.'),
    )

    TICKET_CHOICES = (
        ('single', 'Single'),
        ('return', 'Return'),
        ('season', 'Season ticket'),
    )

    DELAY_TYPE_CHOICES = (
        ('late',
         'It was a direct train and it arrived late.'),

        ('missed_connection',
         'A late train caused me to miss my connection.'),

        ('cancel_before',
         'The train was cancelled before I got on.'),

        ('cancel_during',
         'My stop was cancelled while I was on the train.'),

        ('other',
         'Several of the above, or something else')
    )

    DELAY_MINUTES_CHOICES = (
        (30, '30-59 minutes'),
        (60, '1-2 hours'),
        (120, 'more than 2 hours'),
        (0, "haven't yet arrived or unsure"),
    )

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )

    reference = models.CharField(
        db_index=True,
        null=False,
        max_length=16,
        default=generate_reference,
        editable=False,
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    your_name = models.CharField(
        help_text=(
            'Train companies require your full name.'
        ),
        null=True,
        blank=True,
        max_length=100,
        db_index=True,
    )

    your_email = models.EmailField(
        null=True,
        blank=True,
        help_text=(
            "This is to update you about your claim and optionally remember "
            "your details for next time."
        ),
    )

    your_address = models.TextField(
        null=True,
        blank=True,
        help_text=(
            "The train company will send your payment to this address."
        )
    )

    operating_company = models.ForeignKey(
        'OperatingCompany',
        null=True,
        blank=True,
    )

    submitted_by_user_at = models.DateTimeField(null=True, blank=True)
    submitted_to_operator_at = models.DateTimeField(null=True, blank=True)

    journey_date = models.DateField(null=True, blank=True)

    journey_departure_time = models.TimeField(
        help_text=('In 24 hour format, eg 14:08.'),
        null=True,
        blank=True
    )

    ticket_type = models.CharField(
        null=True,
        blank=True,
        max_length=10,
        choices=TICKET_CHOICES
    )

    travelling_from = models.CharField(
        help_text=(
            'The station where you started your journey.'
        ),
        null=True,
        blank=True,
        max_length=50)

    travelling_to = models.CharField(
        help_text=(
            'Your final destination station.'
        ),
        null=True,
        blank=True,
        max_length=50)

    ticket_face_value = models.DecimalField(
        help_text=(
            'The price printed on the ticket.'
        ),
        null=True,
        blank=True,
        decimal_places=2,
        max_digits=6,  # includes decimal places
    )

    delay_type = models.CharField(
        null=True,
        blank=True,
        max_length=20,
        choices=DELAY_TYPE_CHOICES
    )

    extra_information = models.TextField(
        help_text=(
            "It's very helpful if you provide a few words to backup or "
            "clarify what happened."
        ),
        null=True,
        blank=True
    )

    minutes_delayed = models.PositiveSmallIntegerField(
        null=True,
        choices=DELAY_MINUTES_CHOICES,
    )

    scanned_ticket = VersatileImageField(
        null=True,
        blank=True,
        upload_to=make_scan_filename
    )

    train_uid = models.CharField(
        null=True,
        blank=True,
        max_length=20,
    )

    train_rid = models.CharField(
        null=True,
        blank=True,
        max_length=20,
    )

    cancellation_reason = models.CharField(
        null=True,
        blank=True,
        max_length=200
    )

    delay_reason = models.CharField(
        null=True,
        blank=True,
        max_length=200
    )

    customer_email_token = models.UUIDField(
        default=uuid.uuid4,
        editable=False
    )

    operator_email_token = models.UUIDField(
        default=uuid.uuid4,
        editable=False
    )

    state = models.CharField(
        max_length=50,
        choices=STATE_CHOICES,
        default='INCOMPLETE',
    )

    @property
    def operator_name(self):
        if self.operating_company is None:
            return None
        return self.operating_company.name

    @property
    def operator_email(self):
        if self.operating_company is None:
            return None
        return self.operating_company.email_address

    def can_submit_to_operator_by_email(self):
        operator_accepts_email = True
        return operator_accepts_email and self.state == 'INTERNAL_REVIEW'

    def get_address_lines(self):
        if self.your_address is None:
            return []

        lines = [
            line.strip(' \r\n,') for line in self.your_address.split('\n')
        ]
        return list(filter(None, lines))

    def reply_email_address(self):
        """
        return eg paul-furley.123-456-7890@refundmytrain.com
        """

        return '{}@refundmytrain.com'.format(
            self.get_name_and_reference(separator='.')
        )

    def get_name_and_reference(self, separator='_'):
        """
        return eg 'paul-furley_123-456-1928'
        """

        return '{name}{separator}{reference}'.format(
            separator=separator,
            name=re.sub('[^a-zA-Z]', '-', self.your_name).lower(),
            reference=self.reference,
        )

    def scanned_ticket_as_attachment_triplet(self):
        """
        Return (filename, content, mimetype)

        """

        return (
            self._make_attachment_filename(),
            self._get_tickets_file_content(),
            self._get_mime_type(),
        )

    def _make_attachment_filename(self):
        """
        Return eg scanned_tickets_paul_furley_123-456-7890.jpeg
        """
        return 'scanned_tickets_{name_and_reference}.{extension}'.format(
            name_and_reference=self.get_name_and_reference(separator='_'),
            extension=os.path.splitext(self.scanned_ticket.name)[1]
        )

    def _get_tickets_file_content(self):
        return self.scanned_ticket.read()

    def _get_mime_type(self):
        return None


class OperatingCompany(models.Model):
    slug = models.SlugField(
        primary_key=True
    )

    name = models.CharField(
        max_length=256
    )

    logo = VersatileImageField(upload_to='operator_logos/')

    email_address = models.EmailField()

    def __str__(self):
        return self.name
