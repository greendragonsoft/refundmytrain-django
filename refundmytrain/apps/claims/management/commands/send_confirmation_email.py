import uuid

from django.core.management.base import BaseCommand

from refundmytrain.apps.claims.models import Claim
from refundmytrain.apps.claims.email_helpers import send_confirmation_email


class Command(BaseCommand):
    help = ('(Re)sends a confirmation email to a customer about a claim.')

    def add_arguments(self, parser):
        parser.add_argument('claim_uuid', type=str)

    def handle(self, *args, **options):

        claim_id = uuid.UUID(options['claim_uuid'])
        claim = Claim.objects.get(id=claim_id)

        send_confirmation_email(claim)
