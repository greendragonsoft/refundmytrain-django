# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-06-11 06:45
from __future__ import unicode_literals

from django.db import migrations
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('claims', '0009_claim_operating_company'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='claim',
            name='operator_email',
        ),
        migrations.RemoveField(
            model_name='claim',
            name='operator_name',
        ),
        migrations.AlterField(
            model_name='operatingcompany',
            name='logo',
            field=versatileimagefield.fields.VersatileImageField(upload_to='operator_logos/'),
        ),
    ]
