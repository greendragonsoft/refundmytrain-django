from django.core.urlresolvers import reverse
from django.contrib.admin.views.decorators import staff_member_required
from django.db import transaction
from django.shortcuts import redirect
from django.template import loader
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic.base import View, TemplateView
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.list import ListView
from django.views.generic import DetailView


from .forms import (
    JourneyForm, TicketForm, WhatHappenedForm, YourDetailsForm,
    UploadTicketForm, ReviewForm, EmailTrainCompanyForm, StartPageHiddenForm
)
from .models import Claim, OperatingCompany
from .email_helpers import (
    send_confirmation_email, send_email_to_train_company)


@method_decorator(staff_member_required, name='dispatch')
class AdminListClaims(TemplateView):
    template_name = 'claims/admin/list.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(AdminListClaims, self).get_context_data(*args, **kwargs)
        ctx['claims_incomplete'] = Claim.objects.filter(
            state='INCOMPLETE')
        ctx['claims_waiting'] = Claim.objects.filter(
            state='INTERNAL_REVIEW')
        ctx['claims_all'] = Claim.objects.all()
        return ctx


@method_decorator(staff_member_required, name='dispatch')
class AdminCreateClaim(CreateView):
    model = Claim
    template_name = 'claims/create_claim.html'
    fields = [
        'your_name',
        'your_email',
        'operator_email',
        'travelling_from',
        'travelling_to',
        'ticket_face_value',
        'minutes_delayed',
        'scanned_ticket',
        'train_rid',
    ]


@method_decorator(staff_member_required, name='dispatch')
class AdminManageClaim(DetailView):
    model = Claim
    template_name = 'claims/admin/manage.html'


@method_decorator(staff_member_required, name='dispatch')
class AdminEditClaim(UpdateView):
    model = Claim
    fields = '__all__'
    template_name = 'claims/admin/edit.html'

    def get_success_url(self):
        return reverse('claims.admin.manage',
                       kwargs={'pk': self.get_object().id})


@method_decorator(staff_member_required, name='dispatch')
class AdminEmailTrainCompany(SingleObjectMixin, FormView):
    model = Claim
    template_name = 'claims/admin/email_train_company.html'
    form_class = EmailTrainCompanyForm
    context_object_name = 'claim'

    def get_success_url(self):
        return reverse('claims.admin.manage',
                       kwargs={'pk': self.get_object().id})

    def form_valid(self, form):
        send_email_to_train_company(
            claim=self.get_object(), **form.cleaned_data)

        return super(AdminEmailTrainCompany, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(AdminEmailTrainCompany, self).get(
            request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(AdminEmailTrainCompany, self).post(
            request, *args, **kwargs)

    def get_initial(self):
        initial = super(AdminEmailTrainCompany, self).get_initial()

        claim = self.get_object()

        initial['sender'] = (
            '{} <{}>'.format(
                claim.your_name,
                claim.reply_email_address()))

        initial['to'] = '{} <{}>'.format(
            claim.operator_name, claim.operator_email)

        initial['subject'] = (
            'Delay Repay claim for {travelling_from} to {travelling_to} '
            'on {date}'
        ).format(
            travelling_from=claim.travelling_from,
            travelling_to=claim.travelling_to,
            date=claim.journey_date,
            reference=claim.reference
        )

        initial['message'] = loader.get_template(
            'claims/email/submit_to_train_company.txt').render({
                "claim": claim})

        return initial


@method_decorator(staff_member_required, name='dispatch')
class AdminSubmitToTrainCompanyByEmail(AdminEmailTrainCompany):

    def form_valid(self, form):
        # Sends email:
        result = super(AdminSubmitToTrainCompanyByEmail, self).form_valid(form)

        self._record_claim_submitted()
        return result

    def _record_claim_submitted(self):
        claim = self.get_object()

        claim.submitted_to_operator_at = timezone.now()
        claim.state = 'SUBMITTED'
        claim.save()


class ClaimsHome(ListView):
    model = OperatingCompany
    template_name = 'claims/home.html'


class NextViewNameMixin(object):
    next_view_name = None

    def get_success_url(self):
        if self.next_view_name is None:
            raise RuntimeError('You need to set `next_view_name` on the View '
                               'class in order to determine the next URL on '
                               'successful update.')

        return reverse(self.next_view_name, kwargs={'pk': self.object.id})


class CheckClaimStatusMixin(object):
    """
    This mixin ensures that the Claim is in the "incomplete" state - eg it
    hasn't been submitted - and redirects to the claim status view if it has.

    This prevents a claim from being edited after it has been submitted.
    """

    def dispatch(self, *args, **kwargs):
        claim = self.get_object()
        if claim.state != 'INCOMPLETE':
            return redirect(
                reverse('claim-status', kwargs={'pk': claim.id})
            )

        if not hasattr(self, 'object'):
            self.object = claim  # WTF

        return super(CheckClaimStatusMixin, self).dispatch(*args, **kwargs)


class CreateClaimRedirectToNextView(SingleObjectMixin, View):
    """
    For an existing claim, redirect to the first view for which we don't have
    sufficient or valid data.
    """
    model = Claim

    def get(self, request, pk, *args, **kwargs):
        claim = self.get_object()

        if not claim.travelling_from or not claim.travelling_to:
            view_name = 'create-claim-journey'

        elif not claim.ticket_face_value:
            view_name = 'create-claim-ticket'

        elif claim.minutes_delayed is None or not claim.delay_type:
            view_name = 'create-claim-what-happened'

        elif not claim.your_name or not claim.your_email:
            view_name = 'create-claim-your-details'

        elif not claim.scanned_ticket:
            view_name = 'create-claim-upload-ticket'

        elif claim.state == 'INCOMPLETE':
            view_name = 'create-claim-review'

        else:
            view_name = 'claim-status'

        return redirect(reverse(view_name, kwargs={'pk': claim.id}))


class CreateClaimStartPage(NextViewNameMixin, CreateView):
    model = Claim
    form_class = StartPageHiddenForm

    template_name = 'claims/create_claim_start_page.html'
    next_view_name = 'create-claim-journey'

    def get_initial(self):
        initial = super(CreateClaimStartPage, self).get_initial()

        for field_name in ('travelling_to', 'operating_company'):
            initial[field_name] = self.request.GET.get(
                field_name, None)

        return initial



class CreateClaimJourney(CheckClaimStatusMixin, NextViewNameMixin, UpdateView):
    model = Claim
    form_class = JourneyForm
    template_name = 'claims/create_claim_journey.html'
    next_view_name = 'create-claim-ticket'


class CreateClaimTicket(CheckClaimStatusMixin, NextViewNameMixin, UpdateView):
    model = Claim
    form_class = TicketForm
    template_name = 'claims/create_claim_ticket.html'

    next_view_name = 'create-claim-what-happened'


class CreateClaimWhatHappened(CheckClaimStatusMixin, NextViewNameMixin,
                              UpdateView):
    model = Claim
    form_class = WhatHappenedForm
    template_name = 'claims/create_claim_what_happened.html'
    next_view_name = 'create-claim-your-details'

    def get_context_data(self, *args, **kwargs):
        context = super(CreateClaimWhatHappened, self).get_context_data(
            *args, **kwargs)

        # claim = self.get_object()

        # if claim.expected_arrival_datetime:
        #     dt = claim.expected_arrival_datetime

        #     if dt is not None:
        #         context['override_radio_labels'] = {
        #             'minutes_delayed': {
        #                 "30": 'between {} and {}'.format(
        #                     (dt + datetime.timedelta(minutes=30)).strftime('%H:%M'),
        #                     (dt + datetime.timedelta(minutes=59)).strftime('%H:%M')
        #                 ),
        #                 "60": 'between {} and {}'.format(
        #                     (dt + datetime.timedelta(minutes=60)).strftime('%H:%M'),
        #                     (dt + datetime.timedelta(minutes=119)).strftime('%H:%M')
        #                 ),
        #                 "120": 'after {}'.format(
        #                     (dt + datetime.timedelta(minutes=120)).strftime('%H:%M'),
        #                 ),
        #             }
        #         }
        return context


class CreateClaimYourDetails(CheckClaimStatusMixin, NextViewNameMixin,
                             UpdateView):
    model = Claim
    form_class = YourDetailsForm
    template_name = 'claims/create_claim_your_details.html'
    next_view_name = 'create-claim-upload-ticket'


class CreateClaimUploadTicket(CheckClaimStatusMixin, NextViewNameMixin,
                              UpdateView):
    model = Claim
    form_class = UploadTicketForm
    template_name = 'claims/create_claim_upload_ticket.html'
    next_view_name = 'create-claim-redirect-to-next'


class CreateClaimReview(CheckClaimStatusMixin, SingleObjectMixin, FormView):
    model = Claim
    template_name = 'claims/review.html'
    form_class = ReviewForm

    def form_valid(self, form):
        self._claim = self.get_object()

        self._validate_everything_complete()

        # TODO: check that we're not double submitting

        self._change_state_and_send_email()

        return redirect(
            reverse('claim-status', kwargs={'pk': self.get_object().id})
        )

    def _validate_everything_complete(self):
        pass  # TODO

    def _change_state_and_send_email(self):
        with transaction.atomic():
            self._claim.state = 'INTERNAL_REVIEW'
            self._claim.submitted_by_user_at = timezone.now()
            self._claim.save()

            # TODO: schedule for background
            send_confirmation_email(self._claim)


class ClaimStatus(DetailView):
    model = Claim
    template_name = 'claims/status.html'
