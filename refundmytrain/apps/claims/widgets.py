from django import forms


class HTML5DateInput(forms.DateInput):
    input_type = 'date'

    def __init__(self, attrs=None, format=None, *args, **kwargs):
        if format is None:
            format = '%Y-%m-%d'

        super(HTML5DateInput, self).__init__(attrs, format, *args, **kwargs)
