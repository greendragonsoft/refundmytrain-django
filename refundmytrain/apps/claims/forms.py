from django import forms
from django.forms import ModelForm, RadioSelect, TimeInput
from django.template import loader

from .models import Claim
from .widgets import HTML5DateInput


class OverrideRequiredMixin(object):
    override_fields_required = []
    override_fields_not_required = []

    def __init__(self, *args, **kwargs):
        super(OverrideRequiredMixin, self).__init__(*args, **kwargs)
        for key in self.fields:
            if key in self.override_fields_not_required:
                self.fields[key].required = False

            elif key in self.override_fields_required:
                self.fields[key].required = True


class ExtraAttrsMixin(object):
    extra_attrs = {}

    def __init__(self, *args, **kwargs):
        super(ExtraAttrsMixin, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            attrs = self.extra_attrs.get(field_name, {})

            field.widget.attrs.update(attrs)


def pretty_date():
    from django.utils import timezone
    return timezone.now().date().strftime('%d/%m/%Y')


class StartPageHiddenForm(forms.ModelForm):
    class Meta:
        model = Claim
        fields = ('travelling_to', 'operating_company')
        widgets = {
            'travelling_to': forms.HiddenInput(),
            'operating_company': forms.HiddenInput(),
        }


class JourneyForm(ExtraAttrsMixin, OverrideRequiredMixin, ModelForm):
    class Meta:
        model = Claim
        fields = ('travelling_from', 'travelling_to')

    override_fields_required = (
        'travelling_from',
        'travelling_to',
    )

    extra_attrs = {
        'travelling_from': {
            'placeholder': 'e.g. Liverpool Lime Street',
            'autofocus': '',
        },
        'travelling_to': {'placeholder': 'e.g. Milton Keynes Central'},
    }


class TicketForm(ExtraAttrsMixin, OverrideRequiredMixin, ModelForm):
    class Meta:
        model = Claim
        fields = (
            'ticket_type',
            'ticket_face_value',
            'journey_date',
            'journey_departure_time',
        )

        widgets = {
            'ticket_type': RadioSelect(),
            'journey_date': HTML5DateInput(),
            'journey_departure_time': TimeInput(),
        }

    override_fields_required = (
        'ticket_type',
        'ticket_face_value',
        'journey_date',
        'journey_departure_time',
    )

    extra_attrs = {
        'ticket_type': {'autofocus': ''},
        'journey_date': {'placeholder': 'e.g. {}'.format(pretty_date())},
        'journey_departure_time': {
            'placeholder': 'e.g. 14:08',
            'pattern': "^[0-9]{1,2}[.:]?[0-9]{2}$"
        }
    }


class WhatHappenedForm(ExtraAttrsMixin, OverrideRequiredMixin, ModelForm):
    class Meta:
        model = Claim
        fields = ('delay_type', 'extra_information', 'minutes_delayed')

        widgets = {
            'delay_type': RadioSelect(),
            'minutes_delayed': RadioSelect(),
        }

    extra_attrs = {
        'minutes_delayed': {'autofocus': ''},
        'extra_information': {
            'placeholder': 'e.g.\n\n'
                           'train was 15 minutes late into Crewe, \n'
                           'missed connection to Liverpool, \n'
                           'next train to Liverpool was in an hour',
            'rows': 5,
        }
    }

    override_fields_required = (
        'delay_type',
        'minutes_delayed',
    )


class YourDetailsForm(ExtraAttrsMixin, OverrideRequiredMixin, ModelForm):
    class Meta:
        model = Claim
        fields = ['your_name', 'your_email', 'your_address']

    extra_attrs = {
        'your_name': {
            'placeholder': 'e.g. Michelle Fawkes',
            'autofocus': '',
        },
        'your_email': {'placeholder': 'e.g. m.fawkes67@gmail.com'},
        'your_address': {'rows': '5'}
    }

    override_fields_required = (
        'your_name',
        'your_email',
    )


class UploadTicketForm(ModelForm):
    class Meta:
        model = Claim
        fields = ('scanned_ticket',)


def render_template(template_name, context={}):
    return loader.get_template(template_name).render(context)


class ReviewForm(forms.Form):

    agree_information_correct = forms.BooleanField(
        initial=True,
        required=True,
        label=render_template('claims/partials/agree_information_correct.txt'),
        error_messages={'required': (
            "You need to agree in order to submit a claim.")})

    agree_train_company_terms = forms.BooleanField(
        initial=True,
        required=True,
        label=render_template('claims/partials/agree_train_company_terms.txt'),
        error_messages={'required': (
            "You need to agree in order to submit a claim.")})


class EmailTrainCompanyForm(forms.Form):
    sender = forms.CharField(required=True)
    to = forms.CharField(required=True)
    subject = forms.CharField(required=True)
    bcc = forms.CharField(
        required=False, initial='submit-claim-bcc@refundmytrain.com')

    message = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={'rows': 60})
    )

    attach_ticket = forms.BooleanField(initial=True, required=False)
