from django.contrib import admin

from .models import Claim, OperatingCompany


@admin.register(Claim)
class ClaimAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'updated_at',
        'reference',
        'state',
        'submitted_by_user_at',
        'your_name',
        'your_email',
        'operator_name',
        'operator_email',
        'travelling_from',
        'travelling_to',
        'ticket_face_value',
        'minutes_delayed',
        'scanned_ticket',
        'train_uid',
        'train_rid',
    )


@admin.register(OperatingCompany)
class OperatingCompanyAdmin(admin.ModelAdmin):
    list_display = (
        'slug',
        'name',
        'email_address',
    )
