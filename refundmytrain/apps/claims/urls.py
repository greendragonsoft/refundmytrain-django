from django.conf.urls import url

from refundmytrain.libs.uuid_regex import UUID_PATTERN

from . import views


urlpatterns = [

    url(r'^$',
        views.ClaimsHome.as_view(),
        name='claims.home'),

    url(r'^new/$',
        views.CreateClaimStartPage.as_view(),
        name='create-claim-start-page'),

    url(r'^(?P<pk>' + UUID_PATTERN + ')/journey/$',
        views.CreateClaimJourney.as_view(),
        name='create-claim-journey'),

    url(r'^(?P<pk>' + UUID_PATTERN + ')/ticket/$',
        views.CreateClaimTicket.as_view(),
        name='create-claim-ticket'),

    url(r'^(?P<pk>' + UUID_PATTERN + ')/what-happened/$',
        views.CreateClaimWhatHappened.as_view(),
        name='create-claim-what-happened'),

    url(r'^(?P<pk>' + UUID_PATTERN + ')/your-details/$',
        views.CreateClaimYourDetails.as_view(),
        name='create-claim-your-details'),

    url(r'^(?P<pk>' + UUID_PATTERN + ')/upload-ticket/$',
        views.CreateClaimUploadTicket.as_view(),
        name='create-claim-upload-ticket'),

    url(r'^(?P<pk>' + UUID_PATTERN + ')/$',
        views.CreateClaimRedirectToNextView.as_view(),
        name='create-claim-redirect-to-next'),

    url(r'^(?P<pk>' + UUID_PATTERN + ')/review/',
        views.CreateClaimReview.as_view(),
        name='create-claim-review'),

    url(r'^(?P<pk>' + UUID_PATTERN + ')/status/',
        views.ClaimStatus.as_view(),
        name='claim-status'),

    # Admin views
    url(r'admin/$',
        views.AdminListClaims.as_view(),
        name='claims.admin.list'),

    url(r'admin-new/$',
        views.AdminCreateClaim.as_view(),
        name='claims.admin.create'),

    url(r'admin/edit/(?P<pk>' + UUID_PATTERN + ')/$',
        views.AdminEditClaim.as_view(),
        name='claims.admin.edit'),

    url(r'admin/(?P<pk>' + UUID_PATTERN + ')/$',
        views.AdminManageClaim.as_view(),
        name='claims.admin.manage'),

    url(r'admin/email-train-company/(?P<pk>' + UUID_PATTERN + ')/$',
        views.AdminEmailTrainCompany.as_view(),
        name='claims.admin.email-train-company'),

    url(r'admin/submit-to-train-company/(?P<pk>' + UUID_PATTERN + ')/$',
        views.AdminSubmitToTrainCompanyByEmail.as_view(),
        name='claims.admin.submit-to-train-company-by-email'),
]
