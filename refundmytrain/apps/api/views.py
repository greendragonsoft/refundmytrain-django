from rest_framework.generics import ListAPIView

from refundmytrain.apps.darwinpushport.models import Location

from .serializers import StationSerializer


class StationList(ListAPIView):
    queryset = Location.objects.filter(
        three_alpha__isnull=False,
        is_bus_or_ferry_terminus=False).order_by('name') \
                                       .values('name', 'three_alpha') \
                                       .distinct()

    serializer_class = StationSerializer
