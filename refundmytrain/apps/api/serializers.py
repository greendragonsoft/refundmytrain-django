from rest_framework import serializers

from refundmytrain.apps.darwinpushport.models import Location


class StationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('three_alpha', 'name')
