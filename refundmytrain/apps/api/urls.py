from django.conf.urls import url
from . import views


urlpatterns = [

    url(r'^stations/$',
        views.StationList.as_view(),
        name='api.station-list'),

]
